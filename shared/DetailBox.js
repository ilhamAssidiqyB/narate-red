import styled from '@emotion/styled';
import Image from 'next/image';

const Details = styled.div`
	display: flex;
	justify-content: start;
	align-items: center;
	padding-top: 20px;
`;

const DetailsWrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: start;
`;

const DetailsTitle = styled.h4`
	font-weight: 700;
	font-style: normal;
	font-size: 16px;
	line-height: 20px;
	color: #0f2137;

	@media (min-width: 768px) {
		font-size: 18px;
		line-height: 24px;
	}
`;

const DetailsSubtitle = styled.p`
	font-style: normal;
	font-weight: normal;
	font-size: 12px;
	line-height: 15px;
	color: #343d48;

	@media (min-width: 768px) {
		font-size: 15px;
		line-height: 26px;
	}
`;

const ImageWrapper = styled.div`
	padding-left: 1rem;
	padding-right: 1rem;
`;

const DetailBox = ({ imageSrc, head, subtitle }) => {
	return (
		<Details>
			<ImageWrapper>
				<Image src={imageSrc} width="110px" height="110px" />
			</ImageWrapper>
			<DetailsWrapper>
				<DetailsTitle>{head}</DetailsTitle>
				<DetailsSubtitle>{subtitle}</DetailsSubtitle>
			</DetailsWrapper>
		</Details>
	);
};

export default DetailBox;
