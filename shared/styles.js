import { css, Global, keyframes } from '@emotion/core';
import styled from '@emotion/styled';
import emotionNormalize from 'emotion-normalize';

export const globalStyles = (
	<Global
		styles={css`
		 	${emotionNormalize}
			html, body, * {
				font-family: 'DM Sans', sans-serif;
				box-sizing: border-box;
				margin: 0;
				padding: 0;
				top: 0;
				right: 0;
				left: 0;
			}
		`}
	/>
);

export const Container = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	top: 0;
	left: 0;
	padding-top: 50px;
	padding-bottom: 50px
`;

export const RedContainer =styled(Container)`
	background: linear-gradient(105.18deg, #721e1e -2.04%, #982a2a 107.42%);      
`

export const Wrapper = styled.div`
	width: 90%;
	height: auto;
	display: flex;
	flex-direction: ${({ isReversed }) => (isReversed ? 'column-reverse' : 'column')};
	justify-content: space-between;
	align-items: center;
	margin-top: 1rem;
	margin-bottom: 1rem;

	@media (min-width: 768px){
		width: 100%;
		flex-direction: ${({ isCol }) => (isCol ? 'column' : 'row')};
		width: ${({ isSmaller }) => (isSmaller ? '60%' : '80%')};
	}
`;

export const Title = styled.h3`
	font-weight: 700;
	font-style: normal;
	font-size: 20px;
	line-height: 26px;
	letter-spacing: -1.5px;
	color: ${({isWhite}) => isWhite ? '#fff' : '#0f2137'};
	padding-top: 30px;
	padding-bottom: 30px;

	@media (min-width: 768px) {
		font-size: 36px;
		line-height: 47px;
	}

`;

export const Subtitle = styled.p`
	font-weight: 700;
	font-style: normal;
	font-size: 12px;
	line-height: 16px;
	letter-spacing: 2px;
	text-transform: uppercase;
	color: ${({isWhite}) => isWhite ? '#fff' : '#862424'};

	@media (min-width: 768px) {
		font-size: 14px;
		line-height: 19px;
	}
`;

export const TitleWrapper = styled.div`
	width: 90%;
	display: flex;
	flex-direction: column;
	justify-content: stretch;
	align-items: center;

	@media (min-width: 768px) {
		align-items: ${({ isCentered }) => (isCentered ? 'center' : 'start')};
	}
`;
