import { Container, Subtitle, Title, TitleWrapper, Wrapper } from '../../shared/styles';
import {
	Accordion,
	AccordionItem,
	AccordionItemHeading,
	AccordionItemButton,
	AccordionItemPanel
} from 'react-accessible-accordion';

const Question = () => {
	return (
		<Container>
			<Wrapper isSmaller isCol>
				<TitleWrapper isCentered>
					<Subtitle>Frequent Question</Subtitle>
					<Title>Do you have any question</Title>
				</TitleWrapper>
				<Accordion allowMultipleExpanded allowZeroExpanded>
					<AccordionItem>
						<AccordionItemHeading>
							<AccordionItemButton>How to contact with riders emergency ?</AccordionItemButton>
						</AccordionItemHeading>
						<AccordionItemPanel>
							<p>
								Get your blood tests delivered at the home collect a sample from management news. Get
								your blood tests delivered at the home collect a sample from management news. Get your
								blood tests delivered at the home collect a sample from management news. Get your blood
								tests delivered at the home.
							</p>
						</AccordionItemPanel>
					</AccordionItem>
					<AccordionItem>
						<AccordionItemHeading>
							<AccordionItemButton>
								App installation failed, how to update system information?
							</AccordionItemButton>
						</AccordionItemHeading>
						<AccordionItemPanel>
							<p>
								Get your blood tests delivered at the home collect a sample from management news. Get
								your blood tests delivered at the home collect a sample from management news. Get your
								blood tests delivered at the home collect a sample from management news. Get your blood
								tests delivered at the home.
							</p>
						</AccordionItemPanel>
					</AccordionItem>
					<AccordionItem>
						<AccordionItemHeading>
							<AccordionItemButton>New update fixed all bug and issues</AccordionItemButton>
						</AccordionItemHeading>
						<AccordionItemPanel>
							<p>
								Get your blood tests delivered at the home collect a sample from management news. Get
								your blood tests delivered at the home collect a sample from management news. Get your
								blood tests delivered at the home collect a sample from management news. Get your blood
								tests delivered at the home.
							</p>
						</AccordionItemPanel>
					</AccordionItem>
					<AccordionItem>
						<AccordionItemHeading>
							<AccordionItemButton>Website reponse taking time, how to improve?</AccordionItemButton>
						</AccordionItemHeading>
						<AccordionItemPanel>
							<p>
								Get your blood tests delivered at the home collect a sample from management news. Get
								your blood tests delivered at the home collect a sample from management news. Get your
								blood tests delivered at the home collect a sample from management news. Get your blood
								tests delivered at the home.
							</p>
						</AccordionItemPanel>
					</AccordionItem>
				</Accordion>
			</Wrapper>
		</Container>
	);
};

export default Question;
