import { Container, Subtitle, Title, TitleWrapper, Wrapper } from '../../shared/styles';
import Image from 'next/image';
import { PaymentButton, PaymentContent, PaymentP } from './PaymentElement';

const Payment = () => {
	return (
		<Container>
			<Wrapper isReversed>
				<PaymentContent>
					<Image src="/payment.svg" width="743px" height="820px" />
				</PaymentContent>
				<PaymentContent>
					<TitleWrapper>
						<Subtitle>Core features</Subtitle>
						<Title>Secure Payment Transaction System with #1 Ranking</Title>
					</TitleWrapper>
					<PaymentP>
						Get your tests delivered at let home collect sample from the victory of the managments that
						supplies best design system guidelines ever. Get your tests delivered at let home collect
						sample.
					</PaymentP>
					<PaymentButton>Get Started</PaymentButton>
				</PaymentContent>
			</Wrapper>
		</Container>
	);
};

export default Payment;
