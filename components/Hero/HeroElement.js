import styled from '@emotion/styled';

export const HeroContainer = styled.div`
	width: 100%;
	height: auto;
	display: flex;
	justify-content: center;
	align-items: center;
	top: 0;
	left: 0;
	background: linear-gradient(105.18deg, #721e1e -2.04%, #982a2a 107.42%);
	border-bottom-right-radius: 200px;
`;

export const HeroWrapper = styled.div`
	width: 80%;
	height: auto;
	display: flex;
	justify-content: space-between;
	margin-top: 1rem;
	margin-bottom: 1rem;
`;

export const HeroContent = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-items: start;

	@media (min-width: 768px) {
		width: 50%;
	}
`;

export const HeroImageContent = styled.div`
	display: none;

	@media (min-width: 768px) {
		top: 200px;
		left: 750px;
		position: absolute;
		overflow: hidden;
		overflow-x: hidden;
		display: block;
	}
`;

export const HeroImageWrapper = styled.div`
	display: flex;
`

export const HeroTitle = styled.h1`
	font-weight: 700;
	font-style: normal;
	font-size: 38px;
	line-height: 40px;
	letter-spacing: -2px;
	color: #fff;

	@media (min-width: 768px) {
		font-size: 85px;
		line-height: 100px;
	}
`;

export const HeroSubtitle = styled.p`
	font-weight: 500;
	font-style: normal;
	font-size: 14px;
	line-height: 25px;
	color: #fff;
	margin-top: 1rem;
	margin-bottom: 1rem;

	@media (min-width: 768px) {
		font-size: 18px;
		line-height: 42px;
	}
`;

export const HeroButtonWrapper = styled.div`
	display: flex;
	justify-content: start;
	align-items: center;
	text-align: left;
`;

export const HeroButtonWhite = styled.button`
	width: 150px;
	height: 50px;
	border: 1px solid #fff;
	border-radius: 50px;
	background: #fff;
	color: #370f0f;
	font-weight: 500;
	font-style: normal;
	font-size: 16px;
	line-height: 24px;
	letter-spacing: -0.16px;
	margin-left: 10px;
	margin-right: 10px;
	cursor: pointer;

	&:hover {
		background: transparent;
		transition: 0.3s;
		color: #fff;
	}
`;

export const HeroButtonTransparent = styled.button`
	width: 150px;
	height: 50px;
	background: transparent;
	border: none;
	color: #fff;
	font-weight: 500;
	font-style: normal;
	font-size: 16px;
	line-height: 24px;
	letter-spacing: -0.16px;
	cursor: pointer;
	text-align: center;
	margin-left: 10px;
	margin-right: 10px;

	&:before {
		content: url("/play.svg");
		width: 20px;
		float: left;
		margin-right: 5px;
		margin-top: -2px;
	}

	&:hover {
		border: 1px solid #fff;
		border-radius: 50px;
		transition: 0.3s;
		color: #fff;
	}
`;

export const HeroSponsor = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: flex-start;
	text-align: left;
	margin-top: 1rem;
	margin-bottom: 1rem;

	@media (min-width: 768px) {
		display: flex;
		flex-direction: row;
		justify-content: flex-start;
		align-items: center;
	}
`;

export const HeroSponsorImage = styled.img`
	width: 80px;
	height: 20px;

	@media (min-width: 768px) {
		width: 130px;
		height: 25px;
	}
`;

export const HeroImage = styled.img`max-width: 120%;`;
