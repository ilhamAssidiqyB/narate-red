import {
	HeroButtonTransparent,
	HeroButtonWhite,
	HeroButtonWrapper,
	HeroContainer,
	HeroContent,
	HeroImage,
	HeroImageContent,
	HeroImageWrapper,
	HeroSponsor,
	HeroSponsorImage,
	HeroSubtitle,
	HeroTitle,
	HeroWrapper
} from './HeroElement';

const Hero = () => {
	return (
		<HeroContainer>
			<HeroWrapper>
				<HeroContent>
					<HeroTitle>Experience your ultimate mobile application</HeroTitle>
					<HeroSubtitle>
						Get your blood tests delivered at let home collect sample from the victory of the managments
						that supplies best design system guidelines ever.
					</HeroSubtitle>
					<HeroButtonWrapper>
						<HeroButtonWhite>Get Started</HeroButtonWhite>
						<HeroButtonTransparent>Watch Video</HeroButtonTransparent>
					</HeroButtonWrapper>
					<HeroSponsor>
						<HeroSubtitle>Sponsored:</HeroSubtitle>
						<HeroImageWrapper>
							<HeroSponsorImage src="/paypal.svg" alt="Paypal Logo" />
							<HeroSponsorImage src="/google.svg" alt="Google Logo" />
							<HeroSponsorImage src="/dropbox.svg" alt="Dropbox Logo" />
						</HeroImageWrapper>
					</HeroSponsor>
				</HeroContent>
				<HeroContent>
					<HeroImageContent>
						<HeroImage src="/mockup.svg" alt="Phone Illustration" layout="fill" />
					</HeroImageContent>
				</HeroContent>
			</HeroWrapper>
		</HeroContainer>
	);
};

export default Hero;
