import styled from '@emotion/styled';

export const FeatureWrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	@media (min-width: 768px) {
		flex-direction: row;
	}
`;

export const FeatureDetails = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	padding-left: 0.5rem;
	padding-right: 0.5rem;
`;

export const FeatureTitle = styled.h4`
	font-weight: 700;
	font-style: normal;
	font-size: 18px;
	line-height: 30px;
	color: #0f2137;
`;

export const FeatureSubtitle = styled.p`
	font-weight: 400;
	font-style: normal;
	font-size: 15px;
	line-height: 28px;
	color: #343d48;
	text-align: center;
`;
