import {
	FeatureDetails,
	FeatureSubtitle,
	FeatureTitle,
	FeatureWrapper,
} from './QualityFeaturesElement';
import Image from 'next/image';
import { Container, Subtitle, Title, TitleWrapper, Wrapper } from '../../shared/styles';

const QualityFeatures = () => {
	return (
		<Container>
			<Wrapper isCol isSmaller>
				<TitleWrapper isCentered>
					<Subtitle>Quality features</Subtitle>
					<Title>Meet exciting feature of app</Title>
				</TitleWrapper>
				<FeatureWrapper>
					<FeatureDetails>
						<Image src="/icon-1.svg" width="110px" height="110px" />
						<FeatureTitle>Vector Editing</FeatureTitle>
						<FeatureSubtitle>
							Get your blood tests delivered athome collect a sample from theyour blood tests.
						</FeatureSubtitle>
					</FeatureDetails>
					<FeatureDetails>
						<Image src="/icon-2.svg" width="110px" height="110px" />
						<FeatureTitle>Customize & Monitoring</FeatureTitle>
						<FeatureSubtitle>
							Get your blood tests delivered athome collect a sample from theyour blood tests.
						</FeatureSubtitle>
					</FeatureDetails>
					<FeatureDetails>
						<Image src="/icon-3.svg" width="110px" height="110px" />
						<FeatureTitle>Quality & short-period</FeatureTitle>
						<FeatureSubtitle>
							Get your blood tests delivered athome collect a sample from theyour blood tests.
						</FeatureSubtitle>
					</FeatureDetails>
				</FeatureWrapper>
			</Wrapper>
		</Container>
	);
};

export default QualityFeatures;
