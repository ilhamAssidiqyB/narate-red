import { Container, Subtitle, Title, TitleWrapper, Wrapper } from '../../shared/styles';
import Testimonial from './Testimonial';
import { AllTestimonialsWrapper } from './TestimonialElement';

const Testimonials = () => {
	return (
		<Container>
			<Wrapper isCol>
				<TitleWrapper isCentered>
					<Subtitle>TESTIMONIALS</Subtitle>
					<Title>MEET CLIENT SATISFICATION</Title>
				</TitleWrapper>
				<AllTestimonialsWrapper>
					<Testimonial />
					<Testimonial />
					<Testimonial />
				</AllTestimonialsWrapper>
			</Wrapper>
		</Container>
	);
};

export default Testimonials;
