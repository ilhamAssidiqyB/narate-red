import styled from '@emotion/styled';

export const TestimonialBox = styled.div`
	width: 300px;
	height: auto;
	display: flex;
	justify-content: center;
	align-items: center;
	flex-wrap: wrap;
	word-break: break-all;
	white-space: normal;
	-webkit-box-shadow: 10px 10px 36px -6px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: 10px 10px 36px -6px rgba(0, 0, 0, 0.75);
	box-shadow: 10px 10px 36px -6px rgba(0, 0, 0, 0.75);
	margin: 1rem;
	border-radius: 50px;

	@media (min-width: 768px){
		width: 400px;
	}
`;

export const TestimonialWrapper = styled.div`
	padding: 30px;
	word-break: break-all;
	white-space: normal;
`;

export const TestimonialTitle = styled.h3`
	font-weight: 700;
	font-style: normal;
	font-size: 16px;
	line-height: 33px;
	color: #343d48;
`;

export const TestimonialSubtitle = styled.p`
	font-weight: 400;
	font-style: normal;
	font-size: 18px;
	line-height: 38px;
	color: #343d48;
	word-wrap: break-word;
`;

export const TestimonialProfile = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
`;

export const ProfileName = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: flex-start;
`;
export const Name = styled.h4`
	font-weight: 700;
	font-size: 16px;
	font-style: normal;
	line-height: 40px;
	letter-spacing: -0.4px;
	color: #0f2137;
`;

export const UserName = styled.p`
	font-weight: 500;
	font-style: normal;
	font-size: 15px;
	line-height: 40px;
	letter-spacing: -0.38px;
	color: #25a0ff;
`;

export const AllTestimonialsWrapper = styled.div`
	width: 90%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	

	@media (min-width: 768px) {
		width: 90%;
		display: flex;
		flex-direction: row;
	}
`;
