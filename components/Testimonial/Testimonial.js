import Image from 'next/image';
import {
	Name,
	ProfileName,
	TestimonialBox,
	TestimonialProfile,
	TestimonialSubtitle,
	TestimonialTitle,
	TestimonialWrapper,
	UserName
} from './TestimonialElement';

const Testimonial = () => {
	return (
		<TestimonialBox>
			<TestimonialWrapper>
				<Image src="/stars.svg" alt="5 Stars" width="100px" height="20px" />
				<TestimonialTitle>Modern look & trending design</TestimonialTitle>
				<TestimonialSubtitle>
					Get working experience to work with this amazing team & in future want to work together for bright
					future projects and also make deposit to freelancer.
				</TestimonialSubtitle>
				<TestimonialProfile>
					<Image src="/avatar.svg" width="55px" height="55px" />
					<ProfileName>
						<Name>Denny Hilguston</Name>
						<UserName>@denny.hil</UserName>
					</ProfileName>
				</TestimonialProfile>
			</TestimonialWrapper>
		</TestimonialBox>
	);
};

export default Testimonial;
