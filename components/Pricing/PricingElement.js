import styled from '@emotion/styled';
import { FaCheckCircle } from 'react-icons/fa';
import {AiFillCloseCircle} from 'react-icons/ai'

export const PricingWrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;

	@media (min-width: 768px) {
		display: flex;
		flex-direction: row;
	}
`;

export const PricingBox = styled.div`
	width: 300px;
	height: auto;
	background: ${({ isRed }) => (isRed ? '#922F2F' : '#fff')};
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	padding: 30px;
	margin: 1rem;
	color: ${({ isRed }) => (isRed ? '#fff' : '0f2137')};

	@media (min-width: 768px) {
		width: 550px;
		height: 550px;
	}
`;

export const PricingIcon = styled(FaCheckCircle)`
	color: #3FDBB1
`

export const FreePricingIcon = styled(FaCheckCircle)`
	color: #fff;
	opacity: 0.3;
`

export const PricingTitleWrapper = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: baseline;
`;

export const PricingH3 = styled.h3`
	font-weight: 700;
	font-style: normal;
	font-size: 18px;
	line-height: 23px;
	letter-spacing: -0.55px;
	float: left;

	@media (min-width: 768px) {
		font-size: 22px;
		line-height: 29px;
	}
`;

export const PricingDescription = styled.ul`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: center;
	padding: 10px;
`;

export const PricingList = styled.li`
	font-weight: 400;
	font-style: normal;
	font-size: 12px;
	line-height: 15px;
	margin-top: 0.5rem;
	margin-bottom: 0.5rem;
	list-style: none;

	@media (min-width: 768px) {
		font-size: 16px;
		line-height: 26px;
	}
`;

export const PricingP = styled.p`
	font-weight: 400;
	font-style: normal;
	font-size: 16px;
	line-height: 26px;
`;

export const PricingCost = styled.h2`
	font-weight: 700;
	font-style: normal;
	font-size: 23px;
	line-height: 30px;
	letter-spacing: -0.55px;
	text-align: right;
	color: #25cb9e;
`;

export const PricingButtonWrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
`;

export const PricingButton = styled.button`
	width: 170px;
	height: 50px;
	background: ${({ isWhite }) => (isWhite ? '#fff' : '#761f1f')};
	border: 1px solid #761f1f;
	border: ${({ isWhite }) => (isWhite ? '1px solid #fff' : '1px solid #761f1f')};
	border-radius: 50px;
	font-weight: 500;
	font-style: normal;
	font-size: 16px;
	line-height: 24px;
	letter-spacing: -0.16px;
	text-align: center;
	color: ${({ isWhite }) => (isWhite ? '#922F2F' : '#fff')};

	&:hover {
		background: transparent;
		transition: 0.3s;
		color: ${({ isWhite }) => (isWhite ? '#fff' : '#922F2F')};
	}
`;

export const PricingTag = styled.div`
	width: 110px;
	height: 28px;
	font-weight: bold;
	font-style: normal;
	font-size: 14px;
	line-height: 28px;
	letter-spacing: -0.14px;
	color: #fff;
	background: ${({ isHidden }) => (isHidden ? 'transparent' : '#EF9E48')};
	margin-top: ${({ isHidden }) => (isHidden ? '28px' : '0')};
	text-align: center;
	border-radius: 5px;
`;
