import { RedContainer, Subtitle, Title, TitleWrapper, Wrapper } from '../../shared/styles';
import {
	PricingBox,
	PricingButton,
	PricingButtonWrapper,
	PricingCost,
	PricingDescription,
	PricingIcon,
	PricingH3,
	PricingList,
	PricingP,
	PricingTag,
	PricingTitleWrapper,
	PricingWrapper,
	FreePricingIcon
} from './PricingElement';

const Pricing = () => {
	return (
		<RedContainer>
			<Wrapper isSmaller isCol>
				<TitleWrapper isCentered>
					<Subtitle isWhite>Whats the function</Subtitle>
					<Title isWhite>Let’s see how it works</Title>
				</TitleWrapper>
				<PricingWrapper>
					<PricingBox isRed>
						<PricingTag isHidden />
						<PricingTitleWrapper>
							<PricingH3>Free</PricingH3>
							<PricingP></PricingP>
						</PricingTitleWrapper>
						<PricingTitleWrapper>
							<PricingP>For small teams or office</PricingP>
							<PricingCost></PricingCost>
						</PricingTitleWrapper>
						<PricingDescription>
							<PricingList><PricingIcon /> Ultimate access to all course, exercises and assessments</PricingList>
							<PricingList><PricingIcon /> Free acess for all kind of exercise corrections with downloads.</PricingList>
							<PricingList><PricingIcon /> Total assessment corrections with free download access system</PricingList>
							<PricingList><FreePricingIcon /> Unlimited download of courses on the mobile app contents</PricingList>
							<PricingList><FreePricingIcon /> Download and print courses and exercises in PDF</PricingList>
						</PricingDescription>
						<PricingButtonWrapper>
							<PricingButton isWhite>Signup Now</PricingButton>
						</PricingButtonWrapper>
					</PricingBox>
					<PricingBox>
						<PricingTag>Recomendded</PricingTag>
						<PricingTitleWrapper>
							<PricingH3>Premium</PricingH3>
							<PricingP>Starting From</PricingP>
						</PricingTitleWrapper>
						<PricingTitleWrapper>
							<PricingP>For startup Enterprise</PricingP>
							<PricingCost>49.99/mo</PricingCost>
						</PricingTitleWrapper>
						<PricingDescription>
							<PricingList><PricingIcon /> Ultimate access to all course, exercises and assessments</PricingList>
							<PricingList><PricingIcon /> Free acess for all kind of exercise corrections with downloads.</PricingList>
							<PricingList><PricingIcon /> Total assessment corrections with free download access system</PricingList>
							<PricingList><PricingIcon /> Unlimited download of courses on the mobile app contents</PricingList>
							<PricingList><PricingIcon /> Download and print courses and exercises in PDF</PricingList>
						</PricingDescription>
						<PricingButtonWrapper>
							<PricingButton>Signup Now</PricingButton>
						</PricingButtonWrapper>
					</PricingBox>
				</PricingWrapper>
			</Wrapper>
		</RedContainer>
	);
};

export default Pricing;
