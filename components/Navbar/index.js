import { FaBars } from 'react-icons/fa';
import {
	MobileIcon,
	NavbarButton,
	NavbarContainer,
	NavbarList,
	NavbarListWrapper,
	NavbarLogo,
	NavbarWrapper
} from './NavbarElement';

const Navbar = ({toggle}) => {
	return (
		<NavbarContainer>
			<NavbarWrapper>
				<NavbarLogo>Logo Here</NavbarLogo>
				<NavbarListWrapper>
					<NavbarList>Home</NavbarList>
					<NavbarList>Adversite</NavbarList>
					<NavbarList>Supports</NavbarList>
					<NavbarList>Contacts</NavbarList>
				</NavbarListWrapper>
				<NavbarButton>Get Started</NavbarButton>
				<MobileIcon onClick={toggle}>
					<FaBars />
				</MobileIcon>
			</NavbarWrapper>
		</NavbarContainer>
	);
};

export default Navbar;
