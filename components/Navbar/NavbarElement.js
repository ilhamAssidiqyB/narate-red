import styled from '@emotion/styled';

export const NavbarContainer = styled.nav`
	width: 100%;
	height: auto;
	display: flex;
	justify-content: center;
	align-items: center;
	top: 0;
	left: 0;
	z-index: 10;
	position: sticky;
	background: linear-gradient(105.18deg, #721e1e -2.04%, #982a2a 107.42%);
`;

export const NavbarWrapper = styled.div`
	width: 80%;
	height: auto;
	display: flex;
	justify-content: space-between;
	align-items: center;
	margin-top: 1rem;
	margin-bottom: 1rem;
`;

export const NavbarLogo = styled.h3`
	font-weight: 700;
	font-style: normal;
	font-size: 20px;
	line-height: 24px;
	color: #fff;
`;

export const MobileIcon = styled.div`
	display: block;
	top: 0;
	right: 0;
	font-size: 1.8rem;
	cursor: pointer;
	color: #fff;

	@media (min-width: 768px) {
		display: none;
	}
`;

export const NavbarListWrapper = styled.ul`
	display: none;

	@media (min-width: 768px) {
		display: flex;
		justify-content: center;
		align-items: center;
	}
`;

export const NavbarList = styled.li`
	font-weight: 400;
	font-style: normal;
	font-size: 16px;
	line-height: 33px;
	text-align: center;
	color: #fff;
	list-style: none;
	margin-left: 2rem;
	margin-right: 2rem;
`;

export const NavbarButton = styled.button`
	width: 150px;
	height: 50px;
	border: 1.5px solid #ffffff;
	box-sizing: border-box;
	border-radius: 50px;
	color: #fff;
	font-weight: 500;
	font-style: normal;
	font-size: 16px;
	line-height: 24px;
	letter-spacing: -0.16px;
	background: transparent;

	&:hover {
		background: #fff;
		transition: 0.3s;
		color: #922f2f;
	}
`;
