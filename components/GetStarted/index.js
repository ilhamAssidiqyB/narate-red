import Image from 'next/image';
import { Container, Subtitle, Title, TitleWrapper, Wrapper } from '../../shared/styles';
import { GetStartedButton, GetStartedContent, GetStartedP } from './GetStartedElement';

const GetStarted = () => {
	return (
		<Container>
			<Wrapper>
				<GetStartedContent>
					<TitleWrapper>
						<Subtitle>Core features</Subtitle>
						<Title>Smart Jackpots that you may love this anytime & anywhere</Title>
					</TitleWrapper>
					<GetStartedP>
						Get your tests delivered at let home collect sample from the victory of the managments that
						supplies best design system guidelines ever. Get your tests delivered at let home collect
						sample.
					</GetStartedP>
					<GetStartedButton>Get Started</GetStartedButton>
				</GetStartedContent>
				<GetStartedContent>
					<Image src="/tech.svg" width="800px" height="550px" />
				</GetStartedContent>
			</Wrapper>
		</Container>
	);
};

export default GetStarted;
