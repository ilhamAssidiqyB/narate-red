import styled from '@emotion/styled';

export const GetStartedContent = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	@media (min-width: 768px){
		width: 50%;
	}
`;

export const GetStartedP = styled.p`
	font-weight: 400;
	font-style: normal;
	font-size: 14px;
	line-height: 18px;
	color: #02073e;

	@media (min-width: 768px) {
		font-size: 18px;
		line-height: 42px;
	}
`;

export const GetStartedButton = styled.button`
	width: 150px;
	height: 50px;
	border-radius: 50px;
	border: 1px solid #761f1f;
	background: #761f1f;
	font-weight: 500;
	font-style: normal;
	font-size: 16px;
	line-height: 24px;
	letter-spacing: -0.16px;
	color: #fff;

	&:hover {
		background: transparent;
		transition: 0.3s;
		color: #761f1f;
	}
`;
