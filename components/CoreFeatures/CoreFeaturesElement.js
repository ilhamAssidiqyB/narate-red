import styled from '@emotion/styled';

export const CoreContent = styled.div`
	width: 100%;
	display: flex;
    flex-direction: column;
	justify-content: center;
	align-items: center;

	@media (min-width: 768px){
		width: 50%;
	}
`;


