import Image from 'next/image';
import { Container, Subtitle, Title, TitleWrapper, Wrapper } from '../../shared/styles';
import DetailBox from '../../shared/DetailBox';

import { CoreContent } from './CoreFeaturesElement';

const CoreFeatures = () => {
	return (
		<Container>
			<Wrapper isSmaller isReversed>
				<CoreContent>
					<Image src="/phone-illustrator.svg" width="479px" height="837px" />
				</CoreContent>
				<CoreContent>
					<TitleWrapper>
						<Subtitle>Core features</Subtitle>
						<Title>Smart Jackpotsthat you may love this anytime & anywhere</Title>
					</TitleWrapper>
					<DetailBox
						imageSrc="/icon-4.svg"
						head="Smart Features"
						subtitle="Get your blood tests delivered at let home collect sample from the victory of the managments.your blood tests."
					/>
					<DetailBox
						imageSrc="/icon-5.svg"
						head="Secure Contents"
						subtitle="Get your blood tests delivered at let home collect sample from the victory of the managments.your blood tests."
					/>
				</CoreContent>
			</Wrapper>
		</Container>
	);
};

export default CoreFeatures;
