import { Container, Subtitle, Title, TitleWrapper, Wrapper } from '../../shared/styles';
import DetailBox from '../../shared/DetailBox';
import { AllFeaturesGrid } from './AllFeaturesElement';
const AllFeatures = () => {
	return (
		<Container>
			<Wrapper isCol>
				<TitleWrapper isCentered>
					<Title>Smart Features</Title>
					<Subtitle>Meet exciting feature of app</Subtitle>
				</TitleWrapper>
				<AllFeaturesGrid>
					<DetailBox
						imageSrc="/icon-6.svg"
						head="Smart Features"
						subtitle="Get your blood tests delivered at let home collect sample from the victory of the managments.your blood tests."
					/>
					<DetailBox
						imageSrc="/icon-7.svg"
						head="Fast Performance"
						subtitle="Get your blood tests delivered at let home collect sample from the victory of the managments.your blood tests."
					/>
					<DetailBox
						imageSrc="/icon-8.svg"
						head="Unlimited Content"
						subtitle="Get your blood tests delivered at let home collect sample from the victory of the managments.your blood tests."
					/>
					<DetailBox
						imageSrc="/icon-9.svg"
						head="Ultimate Customization"
						subtitle="Get your blood tests delivered at let home collect sample from the victory of the managments.your blood tests."
					/>
					<DetailBox
						imageSrc="/icon-10.svg"
						head="Boost Productivity"
						subtitle="Get your blood tests delivered at let home collect sample from the victory of the managments.your blood tests."
					/>
					<DetailBox
						imageSrc="/icon-11.svg"
						head="Customer Support"
						subtitle="Get your blood tests delivered at let home collect sample from the victory of the managments.your blood tests."
					/>
				</AllFeaturesGrid>
			</Wrapper>
		</Container>
	);
};

export default AllFeatures;
