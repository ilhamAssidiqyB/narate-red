import styled from '@emotion/styled'

export const AllFeaturesGrid = styled.div`
    padding-top: 50px;
    display: grid;
    grid-template-rows: repeat(6, 1fr);
    grid-template-columns: 1fr;
    grid-row-gap: 50px;

    @media(min-width: 768px){
        grid-template-columns: repeat(3,1fr);
        grid-template-rows:repeat(2, 1fr);
        grid-row-gap: 50px;
    }
`