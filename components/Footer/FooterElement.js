import styled from '@emotion/styled';

export const FooterWrapper = styled.footer`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: center;
`;

export const FooterGrid = styled.div`
	width: 60%;
	display: grid;
	grid-template-columns: repeat(2, 1fr);
	grid-template-rows: repeat(2, 1fr);

	@media (min-width: 768px) {
		grid-template-columns: repeat(4, 1fr);
	}
`;

export const FooterTitle = styled.h3`
	font-weight: 500;
	font-style: normal;
	font-size: 16px;
	line-height: 30px;
	letter-spacing: -0.5px;
	color: #0f2137;

	@media (min-width: 768px) {
		font-size: 18px;
	}
`;

export const FooterListWrapper = styled.ul`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: flex-start;
`;

export const FooterList = styled.li`
	list-style: none;
	font-weight: 400;
	font-style: normal;
	font-size: 12px;
	line-height: 18px;
	color: #02073e;
	margin-top: 0.1rem;
	margin-bottom: 0.1rem;

	@media (min-width: 768px) {
		font-size: 14px;
		line-height: 35px;
	}
`;
