import { Container, Wrapper } from '../../shared/styles';
import { FooterGrid, FooterList, FooterListWrapper, FooterTitle, FooterWrapper } from './FooterElement';

const Footer = () => {
	return (
		<Container>
			<FooterGrid>
				<FooterWrapper>
					<FooterTitle>About Us</FooterTitle>
					<FooterListWrapper>
						<FooterList>Support Center</FooterList>
						<FooterList>Customer support</FooterList>
						<FooterList>About Us</FooterList>
						<FooterList>Copyright</FooterList>
						<FooterList>Populer Campaign</FooterList>
					</FooterListWrapper>
				</FooterWrapper>
                <FooterWrapper>
					<FooterTitle>Our Information</FooterTitle>
					<FooterListWrapper>
						<FooterList>Return Policy</FooterList>
						<FooterList>Privacy Policy</FooterList>
						<FooterList>Terms & Conditions</FooterList>
						<FooterList>Sitemap</FooterList>
						<FooterList>Store House</FooterList>
					</FooterListWrapper>
				</FooterWrapper>
                <FooterWrapper>
					<FooterTitle>My Account</FooterTitle>
					<FooterListWrapper>
						<FooterList>Press Inquiries</FooterList>
						<FooterList>Social Media</FooterList>
						<FooterList>Directories</FooterList>
						<FooterList>Images & B-roll</FooterList>
						<FooterList>Permissions</FooterList>
					</FooterListWrapper>
				</FooterWrapper>
                <FooterWrapper>
					<FooterTitle>Policy</FooterTitle>
					<FooterListWrapper>
						<FooterList>Application Security</FooterList>
						<FooterList>Software Principles</FooterList>
						<FooterList>Unwanted Software Policy</FooterList>
						<FooterList>Responsible Supply Chain</FooterList>
					</FooterListWrapper>
				</FooterWrapper>
			</FooterGrid>
		</Container>
	);
};

export default Footer;
