import styled from '@emotion/styled';

export const WorkWrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;

	@media (min-width: 768px) {
		display: flex;
		flex-direction: row;
	}
`;

export const WorkDetails = styled.div`
	display: flex;
	align-items: center;
	flex-direction: column;
	padding-left: 0.5rem;
	padding-right: 0.5rem;

	@media (min-width: 768px) {
		align-items: flex-start;
	}
`;

export const WorkTitle = styled.h4`
	font-weight: 700;
	font-style: normal;
	font-size: 18px;
	line-height: 30px;
	color: #fff;
`;

export const WorkSubtitle = styled.p`
	font-weight: 400;
	font-style: normal;
	font-size: 15px;
	line-height: 28px;
	color: #fff;
	text-align: left;
`;
