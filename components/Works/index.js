import Image from 'next/image';
import { RedContainer, Subtitle, Title, TitleWrapper, Wrapper } from '../../shared/styles';
import { WorkDetails, WorkSubtitle, WorkTitle, WorkWrapper } from './WorksElement';

const Works = () => {
	return (
		<RedContainer>
			<Wrapper isCol>
				<TitleWrapper isCentered>
					<Subtitle isWhite>Whats the function</Subtitle>
					<Title isWhite>Let’s see how it works</Title>
				</TitleWrapper>
				<WorkWrapper>
					<WorkDetails>
						<Image src="/number-1.svg" width="50px" height="50px" />
						<WorkTitle>Set disbursement Instructions</WorkTitle>
						<WorkSubtitle>
							Get your blood tests delivered athome collect a sample from theyour blood tests.
						</WorkSubtitle>
					</WorkDetails>
					<WorkDetails>
						<Image src="/number-2.svg" width="50px" height="50px" />
						<WorkTitle>Assembly retrieves funds from your account</WorkTitle>
						<WorkSubtitle>
							Get your blood tests delivered athome collect a sample from theyour blood tests.
						</WorkSubtitle>
					</WorkDetails>
					<WorkDetails>
						<Image src="/number-3.svg" width="50px" height="50px" />
						<WorkTitle>Assembly initiates disbursement</WorkTitle>
						<WorkSubtitle>
							Get your blood tests delivered athome collect a sample from theyour blood tests.
						</WorkSubtitle>
					</WorkDetails>
					<WorkDetails>
						<Image src="/number-4.svg" width="50px" height="50px" />
						<WorkTitle>Customer receives funds payment</WorkTitle>
						<WorkSubtitle>
							Get your blood tests delivered athome collect a sample from theyour blood tests.
						</WorkSubtitle>
					</WorkDetails>
				</WorkWrapper>
			</Wrapper>
		</RedContainer>
	);
};

export default Works;
