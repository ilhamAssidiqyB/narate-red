import { useState } from 'react';
import Head from 'next/head';
import AllFeatures from '../components/AllFeatures';
import CoreFeatures from '../components/CoreFeatures';
import Hero from '../components/Hero';
import Navbar from '../components/Navbar';
import QualityFeatures from '../components/QualityFeatures';
import GetStarted from '../components/GetStarted';
import Works from '../components/Works';
import Payment from '../components/Payment';
import Pricing from '../components/Pricing';
import Footer from '../components/Footer';
import Testimonials from '../components/Testimonial';
import Question from '../components/Question';
import Sidebar from '../components/Sidebar';

const Home = () => {
	const [isOpen, setisOpen] = useState(false);

    const toggle = () => {
        setisOpen(!isOpen)
    }
	return (
		<div>
			<Head>
				<title>Red by Narate</title>
				<link
					href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&display=swap"
					rel="stylesheet"
				/>
			</Head>
			<Navbar toggle={toggle} />
			<Sidebar isOpen={isOpen} toggle={toggle} />
			<Hero />
			<QualityFeatures />
			<CoreFeatures />
			<AllFeatures />
			<GetStarted />
			<Works />
			<Testimonials />
			<Payment />
			<Pricing />
			<Question />
			<Footer />
		</div>
	);
};

export default Home;
